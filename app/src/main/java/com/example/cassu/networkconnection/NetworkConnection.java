package com.example.cassu.networkconnection;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class NetworkConnection {

    private Context mContext;
    private boolean checkingInternet;

    public NetworkConnection(Context mContext) {
        this.checkingInternet = Boolean.valueOf(false);
        this.mContext = mContext;
    }

    public boolean checkNetwork() {
        ConnectivityManager manager = (ConnectivityManager) this.mContext.getSystemService("connectivity");
        NetworkInfo info = manager.getNetworkInfo(1);
        NetworkInfo networkInfo = manager.getNetworkInfo(0);
        if (info.isConnected()) {
            this.checkingInternet = Boolean.valueOf(true);

        }else if (networkInfo.isConnected()){
            this.checkingInternet = Boolean.valueOf(true);
        }else {
            this.checkingInternet =Boolean.valueOf(true);
        }

        return this.checkingInternet;
    }
}
