package com.example.cassu;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.FrameLayout;

import com.example.cassu.activity.EditProfileActivity;
import com.example.cassu.fragment.DashboardFragment;
import com.example.cassu.fragment.OrdersFragment;
import com.example.cassu.fragment.PriceSetupFragment;
import com.example.cassu.fragment.SetupproductFragment;
import com.example.cassu.fragment.StoreSettingFragment;
import com.example.cassu.networkconnection.NetworkConnection;
import com.google.android.material.navigation.NavigationView;

import static android.graphics.Color.WHITE;

public class MainActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private NavigationView view;
    private DrawerLayout drawerLayout;
    private FrameLayout frameLayout;

    private NetworkConnection connection;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = findViewById(R.id.tool_bar);
        drawerLayout = findViewById(R.id.drawer_layout);
        view = findViewById(R.id.navi_view);
        frameLayout = findViewById(R.id.frame_layout);
        toolbar.setTitle(getString(R.string.tool_title));
        connection = new NetworkConnection(MainActivity.this);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            view.setBackgroundColor(getColor(R.color.colorWhite));
        }

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar,
                R.string.open_navigation, R.string.close_navigation);
        toggle.getDrawerArrowDrawable().setColor(WHITE);
        drawerLayout.addDrawerListener(toggle);
        drawerLayout.setDrawerListener(toggle);
        toggle.syncState();




        getSupportFragmentManager().beginTransaction()
                .replace(R.id.frame_layout, new DashboardFragment()).commit();


        view.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                switch (item.getItemId()){
                    case R.id.dash_board:
                        getSupportFragmentManager().beginTransaction()
                                .replace(R.id.frame_layout, new DashboardFragment()).commit();
                        toolbar.setTitle(getString(R.string.tool_title));
                        break;

                    case R.id.edit_profile:
                       /* getSupportFragmentManager().beginTransaction()
                                .replace(R.id.frame_layout, new EditProfileFragment()).commit();
                        toolbar.setTitle(getString(R.string.tool_title_edit));*/
                       startActivity(new Intent(MainActivity.this, EditProfileActivity.class));
                       finish();
                        break;

                    case R.id.store_settings:
                        getSupportFragmentManager().beginTransaction()
                                .replace(R.id.frame_layout, new StoreSettingFragment()).commit();
                        toolbar.setTitle(getString(R.string.tool_title_store_setting));
                        break;

                    case R.id.setup_products:
                        getSupportFragmentManager().beginTransaction()
                                .replace(R.id.frame_layout, new SetupproductFragment()).commit();
                        toolbar.setTitle(getString(R.string.tool_title_product_setup));
                        break;

                    case R.id.price_setup:
                        getSupportFragmentManager().beginTransaction()
                                .replace(R.id.frame_layout, new PriceSetupFragment()).commit();
                        toolbar.setTitle(getString(R.string.tool_title_price_setup));
                        break;

                    case R.id.orders:
                        getSupportFragmentManager().beginTransaction()
                                .replace(R.id.frame_layout, new OrdersFragment()).commit();
                        toolbar.setTitle(getString(R.string.tool_title_order));
                        break;
                }

                drawerLayout.closeDrawer(GravityCompat.START);
                return false;
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            final AlertDialog alertDialog = new AlertDialog.Builder(
                    MainActivity.this).create();

            LayoutInflater inflater = ((Activity) MainActivity.this).getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.close_layout, null);
            alertDialog.setView(dialogView);

           Button close = dialogView.findViewById(R.id.close_app);
           Button cancel = dialogView.findViewById(R.id.cancel_app);

           cancel.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View v) {
                   alertDialog.dismiss();
               }
           });

           close.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View v) {
                   MainActivity.super.onBackPressed();
                   finish();
               }
           });

            Window window = alertDialog.getWindow();
            window.setLayout(600,600);
            alertDialog.show();
        }
    }
}
