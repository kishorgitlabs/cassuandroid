package com.example.cassu.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.cassu.MainActivity;
import com.example.cassu.R;
import com.example.cassu.alertbox.AlertBox;
import com.example.cassu.api.API;
import com.example.cassu.api.RetrofitClient;
import com.example.cassu.api.model.login.Login;
import com.example.cassu.networkconnection.NetworkConnection;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private EditText userName, password;
    private Button loginBtn;
    private TextView forgetPassword;
    private String mobNo, pass, mobileNoforget;

    SharedPreferences myShare;
    SharedPreferences.Editor editor;
    private NetworkConnection connection;

    private AlertBox box;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        toolbar = findViewById(R.id.tool_bar_login);
        toolbar.setTitle(getString(R.string.tool_title_login));
        setSupportActionBar(toolbar);
//        toolbar.setTextAlignment();
        userName = findViewById(R.id.mob_no);
        password = findViewById(R.id.pass);
        loginBtn = findViewById(R.id.login_btn);
        forgetPassword = findViewById(R.id.forget_text);
        connection = new NetworkConnection(LoginActivity.this);
        box = new AlertBox(LoginActivity.this);

        myShare = getSharedPreferences("CASSU", MODE_PRIVATE);




        editor = myShare.edit();

        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mobNo = userName.getText().toString();
                pass = password.getText().toString();

                if (mobNo.equals("")){
                    userName.setError("Enter the valid Mobile Number");
                }else if (pass.equals("")){
                    password.setError("Enter the Password");
                }else {
                    if (connection.checkNetwork()){
                       login();
                    }else {
                        box.loginSuccess("Unable to connect Internet. please check your Internet connection !");
                    }
                }

            }
        });

        forgetPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                forgetPass();
            }
        });
    }

    private void forgetPass() {
        final AlertDialog alertDialog = new AlertDialog.Builder(
                LoginActivity.this).create();

        LayoutInflater inflater = ((Activity) LoginActivity.this).getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.forget_password, null);
        alertDialog.setView(dialogView);
        alertDialog.setCanceledOnTouchOutside(false);

        EditText mobileNo = dialogView.findViewById(R.id.mobile_no_forget);
        Button sent = dialogView.findViewById(R.id.sent_btn_forget);
        Button okForget = dialogView.findViewById(R.id.ok_btn_forget);
        LinearLayout sentLayout = dialogView.findViewById(R.id.sent_forget_layout);
        LinearLayout sendMsg = dialogView.findViewById(R.id.send_msg);
        ImageView close = dialogView.findViewById(R.id.close_froget);

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        mobileNoforget = mobileNo.getText().toString();

        sent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (mobileNoforget.equals("")){
//                   mobileNo.setError("Enter the Registered Mobile Number");
//                }else {
                    sentLayout.setVisibility(View.GONE);
                    sendMsg.setVisibility(View.VISIBLE);
//                }
            }
        });

        okForget.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });


        Window window = alertDialog.getWindow();
        window.setLayout(600,600);
        alertDialog.show();
    }

    private void login() {
        final ProgressDialog loading = ProgressDialog.show(LoginActivity.this, getResources().getString(R.string.app_name), "Loading...", false, false);
        API api = RetrofitClient.getApiService();

        Call<Login> call = api.login(mobNo, pass);

        call.enqueue(new Callback<Login>() {
            @Override
            public void onResponse(Call<Login> call, Response<Login> response) {
                loading.dismiss();
                if (response.body().getResult().equals("Success")){

                    editor.putBoolean("isRegister", true);
                    editor.putString("shopId", String.valueOf(response.body().getData().getId()));
                    editor.putString("shopName", response.body().getData().getShopName());
                    editor.putString("name", response.body().getData().getName());
                    editor.putString("mobileNo", response.body().getData().getMobileNo());
                    editor.putString("password", response.body().getData().getPassword());
                    editor.putString("address", response.body().getData().getAddress());
                    editor.putString("pinCode", response.body().getData().getPincode());
                    editor.putString("shopOwnerImages", response.body().getData().getShopOwnerImages());
                    editor.putString("shopOwnerImage", response.body().getData().getShopOwnerImage());
                    editor.putString("typeOfShop", response.body().getData().getTypeofShop());
                    editor.putString("priority", String.valueOf(response.body().getData().getPriority()));
                    editor.commit();



                    final AlertDialog alertDialog = new AlertDialog.Builder(
                            LoginActivity.this).create();

                    LayoutInflater inflater = ((Activity) LoginActivity.this).getLayoutInflater();
                    View dialogView = inflater.inflate(R.layout.login_success, null);
                    alertDialog.setCanceledOnTouchOutside(false);
                    alertDialog.setView(dialogView);

                    TextView textView = dialogView.findViewById(R.id.msg_success);
                    Button ok = dialogView.findViewById(R.id.ok_btn);
                    textView.setText("Login Successfully");

                    ok.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                            startActivity(intent);
                            finish();
                        }
                    });

                    Window window = alertDialog.getWindow();
                    window.setLayout(600,600);
                    alertDialog.show();

                }else if (response.body().getResult().equals("NotSuccess")) {
                    box.loginSuccess("Please try again later");
                }
            }

            @Override
            public void onFailure(Call<Login> call, Throwable t) {
                loading.dismiss();
                box.loginSuccess(getString(R.string.error_server));
            }
        });
    }
}
