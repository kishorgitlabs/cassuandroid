package com.example.cassu.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.example.cassu.R;
import com.example.cassu.alertbox.AlertBox;
import com.example.cassu.api.API;
import com.example.cassu.api.RetrofitClient;
import com.example.cassu.api.model.storecontact.StoreContact;
import com.example.cassu.networkconnection.NetworkConnection;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StoreContactActivity extends AppCompatActivity {

    private EditText mobileNumber, whatsAppNo, payTMNo, googlePay, amazonPay;
    private Button submitBtn;
    private String MobileNo, WhatsAppNo, PayTmNo, GooglePay, AmazonPay;
    private ImageView back;

    private NetworkConnection connection;
    private AlertBox box;

    SharedPreferences myShare;
    SharedPreferences.Editor editor;
    private String shopId, shopName, name, mobNo, WhatsApp, PayTM, gPay, AmazPay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_store_contact);
        mobileNumber = findViewById(R.id.mobile_number);
        whatsAppNo = findViewById(R.id.whats_app);
        payTMNo = findViewById(R.id.paytm_number);
        googlePay = findViewById(R.id.google_pay);
        amazonPay = findViewById(R.id.amazon_pay);
        submitBtn = findViewById(R.id.submit_store_contact);
        back = findViewById(R.id.back);
        connection = new NetworkConnection(StoreContactActivity.this);
        box = new AlertBox(StoreContactActivity.this);

        myShare = getSharedPreferences("CASSU", MODE_PRIVATE);

        shopId = myShare.getString("shopId", "");
        mobNo = myShare.getString("mobileNo", "");
        shopName = myShare.getString("shopName", "");
        name = myShare.getString("name", "");

        WhatsApp = myShare.getString("whatsApp", "");
        PayTM = myShare.getString("payTM", "");
        gPay = myShare.getString("gPay", "");
        AmazPay = myShare.getString("amazonPay", "");

        editor = myShare.edit();

        mobileNumber.setText(mobNo);
        whatsAppNo.setText(WhatsApp);
        payTMNo.setText(PayTM);
        googlePay.setText(gPay);
        amazonPay.setText(AmazPay);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MobileNo = mobileNumber.getText().toString();
                WhatsAppNo = whatsAppNo.getText().toString();
                PayTmNo = payTMNo.getText().toString();
                GooglePay = googlePay.getText().toString();
                AmazonPay = amazonPay.getText().toString();

                if (MobileNo.equals("")){
                    mobileNumber.setError("Enter the Mobile Number");
                }else if (WhatsAppNo.equals("")){
                    whatsAppNo.setError("Enter the What's App Number");
                }else {
                    checkInternet();
                }

            }
        });
    }

    private void checkInternet() {
        if (connection.checkNetwork()){
            storeContact();
        }else {
            box.loginSuccess("Unable to connect Internet. please check your Internet connection !");
        }
    }

    private void storeContact() {
        final ProgressDialog loading = ProgressDialog.show(StoreContactActivity.this, getResources().getString(R.string.app_name), "Loading...", false, false);
        API api = RetrofitClient.getApiService();

        Call<StoreContact> call = api.storeContact(Integer.parseInt(shopId), shopName, name, MobileNo, WhatsAppNo, PayTmNo, GooglePay, AmazonPay);

        call.enqueue(new Callback<StoreContact>() {
            @Override
            public void onResponse(Call<StoreContact> call, Response<StoreContact> response) {
                loading.dismiss();
                if (response.body().getResult().equals("Success")){
                    editor.putString("whatsApp", response.body().getData().getWhatUpNo());
                    editor.putString("payTM", response.body().getData().getPaytmNo());
                    editor.putString("gPay", response.body().getData().getGPay());
                    editor.putString("amazonPay", response.body().getData().getAmazonPay());
                    editor.commit();

                    box.suucessWithBack("Contact Details Successfully Added");
                }else if (response.body().getResult().equals("Updated")){
                    editor.putString("whatsApp", response.body().getData().getWhatUpNo());
                    editor.putString("payTM", response.body().getData().getPaytmNo());
                    editor.putString("gPay", response.body().getData().getGPay());
                    editor.putString("amazonPay", response.body().getData().getAmazonPay());
                    editor.commit();

                    box.suucessWithBack("Contact Details Successfully Updated");
                }else {
                    box.loginSuccess("Please try agian later");
                }
            }

            @Override
            public void onFailure(Call<StoreContact> call, Throwable t) {
                loading.dismiss();
                box.loginSuccess(getString(R.string.error_server));
            }
        });
    }
}
