package com.example.cassu.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.OpenableColumns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.cassu.MainActivity;
import com.example.cassu.R;
import com.example.cassu.alertbox.AlertBox;
import com.example.cassu.api.API;
import com.example.cassu.api.RetrofitClient;
import com.example.cassu.api.model.editprofle.EditProfle;
import com.example.cassu.fragment.EditProfileFragment;
import com.example.cassu.networkconnection.NetworkConnection;
import com.google.android.material.textfield.TextInputEditText;
import com.myhexaville.smartimagepicker.ImagePicker;
import com.myhexaville.smartimagepicker.OnImagePickedListener;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditProfileActivity extends AppCompatActivity {

    private CheckBox uploadPic;
    boolean checkPic;
    private String id, shopName, name, mobileNo, password, changepassword, image;
    private TextInputEditText storeName, Name, mobNo, pass, changePass;
    private Button updateBtn;
    SharedPreferences myShare;
    SharedPreferences.Editor editor;
    private AlertBox box;

    private ImagePicker picker;
    private String imageName = "";
    private File imageFile;
    private Uri uri;
    ImageView selectImage, back;

    private static final String TAG = EditProfileFragment.class.getSimpleName();

    private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;

    private static final int MEDIA_TYPE_IMAGE = 1;

    private Uri fileUri;
    private NetworkConnection connection;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_edit_profile);
        storeName = findViewById(R.id.store_name);
        Name = findViewById(R.id.name);
        mobNo = findViewById(R.id.mobile_no_edit);
        pass = findViewById(R.id.password);
        changePass = findViewById(R.id.change_pass);
        uploadPic = findViewById(R.id.to_upload_owner_pic);
        updateBtn = findViewById(R.id.update_btn);
        myShare = getSharedPreferences("CASSU", MODE_PRIVATE);
        box = new AlertBox(EditProfileActivity.this);
        back = findViewById(R.id.back);
        connection = new NetworkConnection(EditProfileActivity.this);

        id = myShare.getString("shopId", "");
        shopName = myShare.getString("shopName", "");
        name = myShare.getString("name", "");
        mobileNo = myShare.getString("mobileNo", "");
        password = myShare.getString("password", "");
//        changepassword = myShare.getString("", "");
        image = myShare.getString("shopOwnerImage", "");

        storeName.setText(shopName);
        Name.setText(name);
        mobNo.setText(mobileNo);
        pass.setText(password);


        editor = myShare.edit();

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        uploadPic.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked == true){
                    final AlertDialog alertDialog = new AlertDialog.Builder(
                            EditProfileActivity.this).create();

                    LayoutInflater inflater = ((Activity) EditProfileActivity.this).getLayoutInflater();

                    Toast.makeText(EditProfileActivity.this, "Photo size must be less than 2mb", Toast.LENGTH_LONG).show();

                    View dialogView = inflater.inflate(R.layout.open_camera, null);
                    alertDialog.setView(dialogView);
                    alertDialog.setCanceledOnTouchOutside(false);

                    TextView camera = dialogView.findViewById(R.id.camera);
                    camera.setText("Your Photo");
                    Button okay = (Button) dialogView.findViewById(R.id.open_camera);
                    ImageView close = dialogView.findViewById(R.id.close);
                    Button uploadphoto = dialogView.findViewById(R.id.upload_pic);
                    selectImage = dialogView.findViewById(R.id.owner_image);

//                    if (imageName.equals("") || imageName == null){
                    okay.setText("Take Photo");
                    okay.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            picker = new ImagePicker(EditProfileActivity.this, null, new OnImagePickedListener() {
                                @Override
                                public void onImagePicked(Uri imageUri) {
                                    selectImage.setImageURI(imageUri);
                                    imageName = getImageName(imageUri);
                                    imageFile = getFileFromImage();
                                    uri = imageUri;
                                    okay.setText("Change Photo");
                                    uploadphoto.setVisibility(View.VISIBLE);


                                   /* alertDialog.dismiss();
                                    uploadPic.setChecked(false);*/
                                    if (uri.equals(null)){
                                    }else {

                                    }
                                }
                            });
                            picker.choosePicture(true);
                        }
                    });

                    uploadphoto.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            alertDialog.dismiss();
                        }
                    });

                    /*else {
                        selectImage.setImageResource(Integer.parseInt(image));
                        okay.setText("Ok");
                        okay.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                alertDialog.dismiss();

                                uploadPic.setChecked(false);
                            }
                        });
                    }*/
                    close.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            alertDialog.dismiss();
                            uploadPic.setChecked(false);
                        }
                    });


                    Window window = alertDialog.getWindow();
                    window.setLayout(600, 600);
                    alertDialog.show();
                }else {
                    uploadPic.setChecked(false);
                }
            }
        });

        updateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changepassword = changePass.getText().toString();
                if (changepassword.equals("")){
                    changePass.setError("Enter the new Password");
                }else if (imageName.equals("")){
                    box.loginSuccess("Upload the Your Image");
                }else {
                    checkInternet();
                }
            }
        });

    }

    private void checkInternet() {
        if (connection.checkNetwork()){
            editProfile();
        }else {
            box.loginSuccess("Unable to connect Internet. please check your Internet connection !");
        }
    }

    private void editProfile() {
        final ProgressDialog loading = ProgressDialog.show(EditProfileActivity.this, getResources().getString(R.string.app_name), "Loading...", false, false);
        API api = RetrofitClient.getApiService();

        Call<EditProfle> call = api.editProfile(mobileNo, password, changepassword, imageName);

        call.enqueue(new Callback<EditProfle>() {
            @Override
            public void onResponse(Call<EditProfle> call, Response<EditProfle> response) {
                loading.dismiss();
                if (response.body().getResult().equals("Success")){

                }else if (response.body().getResult().equals("Updated")){

                }
            }

            @Override
            public void onFailure(Call<EditProfle> call, Throwable t) {
                loading.dismiss();
                box.loginSuccess(getString(R.string.error_server));
            }
        });
    }

    private File getFileFromImage() {
        try {
            BitmapDrawable drawable = (BitmapDrawable) selectImage.getDrawable();
            Bitmap bitmap = drawable.getBitmap();
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
            byte[] byteArray = stream.toByteArray();
            File directory = new File(getFilesDir(), "profile");
            if (!directory.exists())
                directory.mkdirs();
            File myappFile = new File(directory
                    + File.separator + imageName);
            FileOutputStream fos = new FileOutputStream(myappFile);
            fos.write(byteArray);
//                        mImageName = File_URL + myappFile.getName();
            return myappFile;
        } catch (Exception e) {
            e.printStackTrace();
            return new File("");
        }
    }

    private String getImageName(Uri imageUri) {
        String result = null;
        if (imageUri.getScheme().equals("content")) {
            Cursor cursor = getContentResolver().query(imageUri, null, null, null, null);
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                }
            } finally {
                cursor.close();
            }
        }
        if (result == null) {
            result = imageUri.getPath();
            int cut = result.lastIndexOf('/');
            if (cut != -1) {
                result = result.substring(cut + 1);
            }
        }
        return result;
    }

    @Override
    public void onBackPressed() {
        uploadPic.setChecked(false);
//        super.onBackPressed();
        startActivity(new Intent(EditProfileActivity.this, MainActivity.class));
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 200) {
            picker.handleActivityResult(resultCode, requestCode, data);
        }
    }
}
