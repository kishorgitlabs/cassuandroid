package com.example.cassu.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.cassu.R;
import com.example.cassu.alertbox.AlertBox;


public class SetMainProductActivity extends AppCompatActivity {

    private TextView shopType;
    private ImageView back;
    private String ShopType;
    private CheckBox addProducts;
    private AlertBox box;

    SharedPreferences myShare;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_main_product);
        back = findViewById(R.id.back);
        shopType = findViewById(R.id.shop_type);
        addProducts = findViewById(R.id.add_main_product);
        box = new AlertBox(SetMainProductActivity.this);
        myShare = getSharedPreferences("CASSU", MODE_PRIVATE);

        ShopType = myShare.getString("typeOfShop", "");

        editor = myShare.edit();

        shopType.setText(ShopType);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        addProducts.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
               /* if (isChecked == true){
                    final AlertDialog alertDialog = new AlertDialog.Builder(
                            SetMainProductActivity.this).create();

                    LayoutInflater inflater = ((Activity) SetMainProductActivity.this).getLayoutInflater();
                    View dialogView = inflater.inflate(R.layout.add_main_products, null);
                    alertDialog.setView(dialogView);

                    alertDialog.show();
                }else if (isChecked = false){

                }*/
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
