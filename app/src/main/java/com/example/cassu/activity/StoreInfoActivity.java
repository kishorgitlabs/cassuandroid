package com.example.cassu.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.cassu.R;
import com.example.cassu.alertbox.AlertBox;
import com.example.cassu.api.API;
import com.example.cassu.api.RetrofitClient;
import com.example.cassu.api.model.storeinfo.StoreInfo;
import com.example.cassu.api.model.storeinformation.StoreInfoView;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.google.android.gms.plus.PlusOneDummyView.TAG;

public class StoreInfoActivity extends AppCompatActivity {

    private EditText shopType, shopName, shopAddr, pinCode, emailId, website, landMark, openingTime, closingTime, hdCharge;
    private String ShopType, ShopName, ShopAddr, PinCode, EmailId, Website, LandMark, OpeningTime, ClosingTime, HomeDelivery, HDCharge, Courier, PickupFacility;
    private String[] type = {"-Select-", "Yes", "No"};
    private Button sumbitBtn;
    private Spinner homeDeli, pickUp, courier;
    private LinearLayout homeDeliveryChargeLayout;

    SharedPreferences myShare;
    SharedPreferences.Editor editor;


    private FusedLocationProviderClient mFusedLocationClient;
    private SettingsClient mSettingsClient;
    private LocationCallback mLocationCallback;
    private Location mCurrentLocation;
    private static final long UPDATE_INTERVAL_IN_MILLISECONDS = 50000;
    private Boolean mRequestingLocationUpdates;
    private LocationRequest mLocationRequest;
    private LocationSettingsRequest mLocationSettingsRequest;
    private static final int REQUEST_CHECK_SETTINGS = 100;
    private static final int REQUEST_CHECK_SETTINGS_GPS = 100;
    private String locationaddress;
    private Address addresss;
    private boolean permissionGranted = true;

    TimePickerDialog pickerDialog;
    Calendar calendar;
    int currentHour;
    int currentMins;
    String amPm;

    int shopId;
    Double hdchare;
    int hdChareView;

    private AlertBox box;
    String setEmail, setWebsite, setLandmark, setOpeningTime, setClosingTime, setHdCharge, setAddr, setPinCode, setCourier, setHomeDelivery, setPickup;

    List<String> selectedCourier, selectedHomeDeli, selectedPickUp;
    int setHomeDeli;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_store_info);
        shopAddr = findViewById(R.id.addressss);
        pinCode = findViewById(R.id.pin_code);
        shopType = findViewById(R.id.shop_type);
        shopName = findViewById(R.id.shop_name);
        emailId = findViewById(R.id.email_id);
        website = findViewById(R.id.website);
        landMark = findViewById(R.id.land_mark);
        openingTime = findViewById(R.id.opening_time);
        closingTime = findViewById(R.id.closing_time);
        hdCharge = findViewById(R.id.home_delivery_charge);
        homeDeli = findViewById(R.id.home_delivery);
        pickUp = findViewById(R.id.pick_up);
        courier = findViewById(R.id.courier);
        homeDeliveryChargeLayout = findViewById(R.id.home_delivery_charge_layout);
        sumbitBtn = findViewById(R.id.submit_store_info);
        myShare = getSharedPreferences("CASSU", MODE_PRIVATE);
        box = new AlertBox(StoreInfoActivity.this);
        selectedHomeDeli = new ArrayList<>();

        shopId = Integer.parseInt(myShare.getString("shopId", ""));
        shopName.setText(myShare.getString("shopName", ""));
        shopType.setText(myShare.getString("typeOfShop", ""));

        setEmail = myShare.getString("emailIdShop", "");
        setWebsite = myShare.getString("websiteShop", "");
        setLandmark = myShare.getString("landMarkShop", "");
        setOpeningTime = myShare.getString("openingTimeShop", "");
        setClosingTime = myShare.getString("closingTimeShop", "");
        setHdCharge = myShare.getString("hdChargeShop", "");
        setAddr = myShare.getString("addressShop", "");
        setPinCode = myShare.getString("pinCodeShop", "");
        setCourier = myShare.getString("courierShop", "");
        setHomeDelivery = myShare.getString("homeDeliShop", "");
        setPickup = myShare.getString("pickUpShop", "");
//        setHomeDeli = myShare.getInt("selectedHomeDeli", 0);

        selectedHomeDeli.add(0, setHomeDelivery);
        ArrayAdapter<String> seletedHomeDeli = new ArrayAdapter<String>(StoreInfoActivity.this, android.R.layout.simple_list_item_1, selectedHomeDeli);
        homeDeli.setAdapter(seletedHomeDeli);


//        homeDeli.setSelection(setHomeDeli);
//        emailId.setText(setEmail);
//        website.setText(setWebsite);
//        landMark.setText(setLandmark);
//        openingTime.setText(setOpeningTime);
//        closingTime.setText(setClosingTime);
////            int sethome = Integer.parseInt(myShare.getString("homeDeli", ""));
////            homeDeli.setSelection(sethome);
//        hdCharge.setText(setHdCharge);
//        shopAddr.setText(setAddr);
//        pinCode.setText(setPinCode);

//        }
        editor = myShare.edit();

        getDetails();

        openingTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(StoreInfoActivity.this, "Tap Again to Set Time", Toast.LENGTH_LONG).show();
                calendar = Calendar.getInstance();
                currentHour = calendar.get(Calendar.HOUR_OF_DAY);
                currentMins = calendar.get(Calendar.MINUTE);

                pickerDialog = new TimePickerDialog(StoreInfoActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        if (hourOfDay >= 12) {
                            amPm = "PM";
                        } else {
                            amPm = "AM";
                        }

                        openingTime.setText(String.format("%02d:%02d", hourOfDay, minute) + " " + amPm);

                    }
                }, currentHour, currentMins, false);
                pickerDialog.show();
            }
        });

        closingTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(StoreInfoActivity.this, "Tap Again to Set Time", Toast.LENGTH_LONG).show();
                calendar = Calendar.getInstance();
                currentHour = calendar.get(Calendar.HOUR_OF_DAY);
                currentMins = calendar.get(Calendar.MINUTE);

                pickerDialog = new TimePickerDialog(StoreInfoActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        if (hourOfDay >= 12) {
                            amPm = "PM";
                        } else {
                            amPm = "AM";
                        }

                        closingTime.setText(String.format("%02d:%02d", hourOfDay, minute) + " " + amPm);

                    }
                }, currentHour, currentMins, false);
                pickerDialog.show();
            }
        });

        ArrayAdapter<String> homeDeliAdapter = new ArrayAdapter<String>(StoreInfoActivity.this, android.R.layout.simple_list_item_1, type);
        homeDeli.setAdapter(homeDeliAdapter);

        ArrayAdapter<String> courierAdapter = new ArrayAdapter<String>(StoreInfoActivity.this, android.R.layout.simple_list_item_1, type);
        courier.setAdapter(courierAdapter);

        ArrayAdapter<String> pickUpAdapter = new ArrayAdapter<String>(StoreInfoActivity.this, android.R.layout.simple_list_item_1, type);
        pickUp.setAdapter(pickUpAdapter);

        homeDeli.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String item = parent.getItemAtPosition(position).toString();
                if (item.equals("Yes")) {
                    homeDeliveryChargeLayout.setVisibility(View.VISIBLE);
                } else if (item.equals("No")) {
                    homeDeliveryChargeLayout.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        courier.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String item = parent.getItemAtPosition(position).toString();
                editor.putInt("selectedCourier", parent.getSelectedItemPosition());
                editor.commit();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        pickUp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String item = parent.getItemAtPosition(position).toString();
                editor.putInt("selectedPickUp", parent.getSelectedItemPosition());
                editor.commit();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        initPermission();

        sumbitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShopType = shopType.getText().toString();
                ShopName = shopName.getText().toString();
                ShopAddr = shopAddr.getText().toString();
                PinCode = pinCode.getText().toString();
                EmailId = emailId.getText().toString();
                Website = website.getText().toString();
                LandMark = landMark.getText().toString();
                OpeningTime = openingTime.getText().toString();
                ClosingTime = closingTime.getText().toString();
                Courier = courier.getSelectedItem().toString();
                HomeDelivery = homeDeli.getSelectedItem().toString();
                HDCharge = hdCharge.getText().toString();
                PickupFacility = pickUp.getSelectedItem().toString();
                if (HDCharge.equalsIgnoreCase("")){
                    hdCharge.setText("0");
                }

                if (ShopAddr.equals("") || ShopName.equals("") || ShopType.equals("")) {
                    shopAddr.setError("Enter the Shop Address");
                    shopName.setError("Enter the Shop Name");
                    shopType.setError("Enter the Shop Type");
                } else if (OpeningTime.equals("") || ClosingTime.equals("")) {
                    openingTime.setError("Set Opening Time for Shop");
                    closingTime.setError("Set Closing Time for Shop");
                } else if (Courier.equals("-Select-")) {
                    Toast.makeText(StoreInfoActivity.this, "Selected Courier Option", Toast.LENGTH_SHORT).show();
                } else if (HomeDelivery.equals("-Select-")) {
                    Toast.makeText(StoreInfoActivity.this, "Selected Home Delivery Option", Toast.LENGTH_SHORT).show();
                } else if (PickupFacility.equals("-Select-")) {
                    Toast.makeText(StoreInfoActivity.this, "Selected Pickup Facility Option", Toast.LENGTH_SHORT).show();
                } else {
                    storeInfo();
                }
            }
        });

    }

    private void getDetails() {
        final ProgressDialog loading = ProgressDialog.show(StoreInfoActivity.this, getResources().getString(R.string.app_name), "Loading...", false, false);
        API api = RetrofitClient.getApiService();

        Call<StoreInfoView> call = api.storeInfoView(String.valueOf(shopId));

        call.enqueue(new Callback<StoreInfoView>() {
            @Override
            public void onResponse(Call<StoreInfoView> call, Response<StoreInfoView> response) {
                loading.dismiss();
                if (response.body().getResult().equals("Success")) {


                    emailId.setText(response.body().getData().getEmailId());
                    website.setText(response.body().getData().getWebsite());
                    landMark.setText(response.body().getData().getLandMark());
                    openingTime.setText(response.body().getData().getOpeningTime());
                    closingTime.setText(response.body().getData().getClosingTime());
                    hdchare = Double.valueOf(String.valueOf(response.body().getData().getHDCharge()));
                    double chargeValue = hdchare.doubleValue();
                    hdChareView = (int) chargeValue;

                    hdCharge.setText(hdChareView+" ");
                    ArrayList couirerList = new ArrayList();
                    couirerList.add(" -Select- ");
                    couirerList.add("Yes");
                    couirerList.add("No");
                    if (couirerList.contains(response.body().getData().getQuerierFacility())){
                        couirerList.remove(response.body().
                                getData().getQuerierFacility());
                    }

                    couirerList.add(0, response.body().getData().getQuerierFacility());
                    ArrayAdapter<String> xxx = new ArrayAdapter<String>(StoreInfoActivity.this, android.R.layout.simple_list_item_1,couirerList);
                    courier.setAdapter(xxx);

                    ArrayList deliveryList = new ArrayList();
                    deliveryList.add(" -Select- ");
                    deliveryList.add("Yes");
                    deliveryList.add("No");
                    if (deliveryList.contains(response.body().getData().getHomeDelivery())){
                        deliveryList.remove(response.body().getData().getHomeDelivery());
                    }

                    deliveryList.add(0, response.body().getData().getHomeDelivery());
                    ArrayAdapter<String> xxxx = new ArrayAdapter<String>(StoreInfoActivity.this, android.R.layout.simple_list_item_1,deliveryList);
                    homeDeli.setAdapter(xxxx);


                    ArrayList pickFacilityList = new ArrayList();
                    pickFacilityList.add(" -Select- ");
                    pickFacilityList.add("Yes");
                    pickFacilityList.add("No");
                    if (pickFacilityList.contains(response.body().getData().getPickupFacility())){
                        pickFacilityList.remove(response.body().getData().getPickupFacility());
                    }

                    pickFacilityList.add(0, response.body().getData().getPickupFacility());
                    ArrayAdapter<String> xxxxx = new ArrayAdapter<String>(StoreInfoActivity.this, android.R.layout.simple_list_item_1,pickFacilityList);
                    pickUp.setAdapter(xxxxx);


                    shopAddr.setText(response.body().getData().getAddress());
                    pinCode.setText(response.body().getData().getPincode());

                }
            }

            @Override
            public void onFailure(Call<StoreInfoView> call, Throwable t) {
                loading.dismiss();
                box.loginSuccess(getString(R.string.error_server));
            }
        });

    }

    private void storeInfo() {
        final ProgressDialog loading = ProgressDialog.show(StoreInfoActivity.this, getResources().getString(R.string.app_name), "Loading...", false, false);
        API api = RetrofitClient.getApiService();

        Call<StoreInfo> call = api.storeInfo(shopId, ShopType, ShopName, ShopAddr, PinCode, EmailId, Website, LandMark, OpeningTime, ClosingTime, HomeDelivery, HDCharge, PickupFacility, Courier);

        call.enqueue(new Callback<StoreInfo>() {
            @Override
            public void onResponse(Call<StoreInfo> call, Response<StoreInfo> response) {
                loading.dismiss();
                if (response.body().getResult().equals("Success")) {
                    editor.putString("typeOfShop", response.body().getData().getTypeOfShop());
                    editor.putString("nameShop", response.body().getData().getShopName());
                    editor.putString("addressShop", response.body().getData().getAddress());
                    editor.putString("pinCodeShop", response.body().getData().getPincode());
                    editor.putString("emailIdShop", response.body().getData().getEmailId());
                    editor.putString("websiteShop", response.body().getData().getWebsite());
                    editor.putString("landMarkShop", response.body().getData().getLandMark());
                    editor.putString("openingTimeShop", String.valueOf(response.body().getData().getOpeningTime()));
                    editor.putString("closingTimeShop", String.valueOf(response.body().getData().getClosingTime()));
                    editor.putInt("courierShop", (Integer) response.body().getData().getCourier());
                    editor.putString("homeDeliShop", response.body().getData().getHomeDelivery());
                    editor.putString("hdChargeShop", String.valueOf(response.body().getData().getHDCharge()));
                    editor.putString("pickUpShop", response.body().getData().getPickupFacility());
                    editor.putString("gpsLocShop", String.valueOf(response.body().getData().getGpsLocation()));
                    editor.apply();
                    editor.commit();
                    box.loginSuccess("Store Information Successfully Added");

                } else if (response.body().getResult().equals("Updated")) {
                    editor.putString("typeOfShop", response.body().getData().getTypeOfShop());
                    editor.putString("nameShop", response.body().getData().getShopName());
                    editor.putString("addressShop", response.body().getData().getAddress());
                    editor.putString("pinCodeShop", response.body().getData().getPincode());
                    editor.putString("emailIdShop", response.body().getData().getEmailId());
                    editor.putString("websiteShop", response.body().getData().getWebsite());
                    editor.putString("landMarkShop", response.body().getData().getLandMark());
                    editor.putString("openingTimeShop", String.valueOf(response.body().getData().getOpeningTime()));
                    editor.putString("closingTimeShop", String.valueOf(response.body().getData().getClosingTime()));
                    editor.putString("courierShop", String.valueOf(response.body().getData().getCourier()));
                    editor.putString("selectedHomeDeli", response.body().getData().getHomeDelivery());
                    editor.putString("hdChargeShop", String.valueOf(response.body().getData().getHDCharge()));
                    editor.putString("pickUpShop", response.body().getData().getPickupFacility());
                    editor.putString("gpsLocShop", String.valueOf(response.body().getData().getGpsLocation()));
                    editor.apply();
                    editor.commit();

                    box.loginSuccess("Store Information Successfully Updated");

                }
            }

            @Override
            public void onFailure(Call<StoreInfo> call, Throwable t) {
                loading.dismiss();
                box.loginSuccess(getString(R.string.error_server));

            }
        });

    }

    private void initPermission() {
        if (PackageManager.PERMISSION_GRANTED != ActivityCompat.checkSelfPermission(StoreInfoActivity.this, Manifest.permission.ACCESS_FINE_LOCATION)) {
            ActivityCompat.requestPermissions(StoreInfoActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 100);
        } else {
            permissionGranted = true;
            init();
            startLocationUpdate();
        }
    }

    private void startLocationUpdate() {
        mSettingsClient
                .checkLocationSettings(mLocationSettingsRequest)
                .addOnSuccessListener(this, new OnSuccessListener<LocationSettingsResponse>() {
                    @SuppressLint("MissingPermission")
                    @Override
                    public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                        Log.i(TAG, "All location settings are satisfied.");


                        //noinspection MissingPermission
                        mFusedLocationClient.requestLocationUpdates(mLocationRequest,
                                mLocationCallback, Looper.myLooper());


                        updateLocationUI();
                    }
                })
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        int statusCode = ((ApiException) e).getStatusCode();
                        switch (statusCode) {
                            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                                Log.i(TAG, "Location settings are not satisfied. Attempting to upgrade " +
                                        "location settings ");
                                try {
                                    // Show the dialog by calling startResolutionForResult(), and check the
                                    // result in onActivityResult().
                                    ResolvableApiException rae = (ResolvableApiException) e;
                                    rae.startResolutionForResult(StoreInfoActivity.this, REQUEST_CHECK_SETTINGS);
                                } catch (IntentSender.SendIntentException sie) {
                                    Log.i(TAG, "PendingIntent unable to execute request.");
                                }
                                break;
                            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                                String errorMessage = "Location settings are inadequate, and cannot be " +
                                        "fixed here. Fix in Settings.";
                                Log.e(TAG, errorMessage);

                                Toast.makeText(StoreInfoActivity.this, errorMessage, Toast.LENGTH_LONG).show();
                                break;
                            case LocationSettingsStatusCodes.SUCCESS:
                                permissionGranted = true;
                                init();
                        }
                        updateLocationUI();
                    }
                });
    }

    private void updateLocationUI() {
        if (mCurrentLocation != null) {
            new GeocodeAsyncTask().execute(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude());
        }
    }

    @SuppressLint("RestrictedApi")
    private void init() {
        if (permissionGranted) {
            mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
            mSettingsClient = LocationServices.getSettingsClient(this);

            mLocationCallback = new LocationCallback() {
                @Override
                public void onLocationResult(LocationResult locationResult) {
                    super.onLocationResult(locationResult);

                    mCurrentLocation = locationResult.getLastLocation();
                    updateLocationUI();
                }
            };

            mRequestingLocationUpdates = false;
            mLocationRequest = new LocationRequest();
            mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
            mLocationRequest.setFastestInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
            mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
            builder.addLocationRequest(mLocationRequest);
            mLocationSettingsRequest = builder.build();
        }
    }

    private class GeocodeAsyncTask extends AsyncTask<Double, Void, Address> {

        String errorMessage = "";

        @Override
        protected Address doInBackground(Double... latlang) {
            Geocoder geocoder = new Geocoder(StoreInfoActivity.this, Locale.getDefault());
            List<Address> addresses = null;
            if (geocoder.isPresent()) {
                try {
                    addresses = geocoder.getFromLocation(latlang[0], latlang[1], 1);

                    Log.d(TAG, "doInBackground: ************");
                } catch (IOException ioException) {
                    errorMessage = "Service Not Available";
                    Log.e(TAG, errorMessage, ioException);
                } catch (IllegalArgumentException illegalArgumentException) {
                    errorMessage = "Invalid Latitude or Longitude Used";
                    Log.e(TAG, errorMessage + ". " +
                            "Latitude = " + latlang[0] + ", Longitude = " +
                            latlang[1], illegalArgumentException);
                }
                if (addresses != null && addresses.size() > 0)

                    return addresses.get(0);


            }

            return null;
        }

        @Override
        protected void onPostExecute(Address address) {
            if (address == null) {
            } else {
                String addresssLine = address.getAddressLine(0);
                String pin = address.getPostalCode();

                shopAddr.setText(addresssLine);

                pinCode.setText(pin);

                Geocoder geocoder = new Geocoder(StoreInfoActivity.this);
                try {
                    ArrayList<Address> addresses = (ArrayList<Address>) geocoder.getFromLocationName("karur", 50);
                    for (Address address3 : addresses) {
                        double lat = address3.getLatitude();
                        double lon = address3.getLongitude();
                    }
                } catch (IOException e) {
                    e.printStackTrace();

                }
            }
        }
    }
}
