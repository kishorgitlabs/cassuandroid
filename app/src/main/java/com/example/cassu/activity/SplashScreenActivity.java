package com.example.cassu.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.cassu.MainActivity;
import com.example.cassu.R;
import com.example.cassu.alertbox.AlertBox;
import com.example.cassu.networkconnection.NetworkConnection;

import java.util.Timer;
import java.util.TimerTask;

public class SplashScreenActivity extends AppCompatActivity {

    private Timer timer;
    private ProgressBar progressBar;
    private int i = 0;
    TextView textView, networkCheck;

    SharedPreferences myShare;
    SharedPreferences.Editor editor;
    private boolean isRegister;

    private AlertBox box;

    private NetworkConnection connection;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        progressBar = findViewById(R.id.progressBar);
        textView = findViewById(R.id.loading);
        networkCheck = findViewById(R.id.network_check);
        box = new AlertBox(SplashScreenActivity.this);
        connection = new NetworkConnection(SplashScreenActivity.this);

        myShare = getSharedPreferences("CASSU", MODE_PRIVATE);

        isRegister = myShare.getBoolean("isRegister", false);


        editor = myShare.edit();

        progressBar.setProgress(0);

        final long period = 100;

        timer = new Timer();

        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                if (i < 100){
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            textView.setText("Loading...");
                        }
                    });
                    progressBar.setProgress(i);
                    i=i+5;
                }else {
                    timer.cancel();
                    if (connection.checkNetwork()){
                       if (isRegister == true){
                           Intent intent = new Intent(SplashScreenActivity.this, MainActivity.class);
                           startActivity(intent);
                           finish();
                       }else {
                           Intent intent = new Intent(SplashScreenActivity.this, LoginActivity.class);
                           startActivity(intent);
                           finish();
                       }
                    }else {
                        box.loginSuccess("Unable to connect Internet. please check your Internet connection !");
                    }

                }

            }
        },0, period);
    }
}
