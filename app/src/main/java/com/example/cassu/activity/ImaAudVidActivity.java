package com.example.cassu.activity;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.cassu.R;
import com.example.cassu.adapter.ImagesAdapter;

import java.util.ArrayList;

public class ImaAudVidActivity extends AppCompatActivity {

    private String toolBarView;
    private ImageView back;
    private TextView toolBarTextView;
    private ProgressBar mProgressBar;
    private int REQUEST_CODE_READ_STORAGE = 200, RESULT_OK = -1;

    private LinearLayout uploadImagesLayout, uploadVideoLayout;
    //image upload
    private ListView imagesList;
    private Button chooseImageBtn, uploadImageBtn;
    private ArrayList<Uri> arrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ima_aud_vid);
        back = findViewById(R.id.back);
        toolBarTextView = findViewById(R.id.textView_toolbar);
        uploadImagesLayout = findViewById(R.id.image_upload_layout);
        uploadVideoLayout = findViewById(R.id.video_upload_layout);
        imagesList = findViewById(R.id.images_list);
        chooseImageBtn = findViewById(R.id.choose_image_btn);
        uploadImageBtn = findViewById(R.id.upload_image_btn);
        uploadVideoLayout.setVisibility(View.GONE);
        uploadImagesLayout.setVisibility(View.GONE);
        arrayList = new ArrayList<>();

        toolBarView = getIntent().getStringExtra("className");

        toolBarTextView.setText(toolBarView);

        if (toolBarView.equals("Store Images")){
            uploadImagesLayout.setVisibility(View.VISIBLE);
            uploadVideoLayout.setVisibility(View.GONE);
        }else if (toolBarView.equals("Store Videos")){
            uploadImagesLayout.setVisibility(View.GONE);
            uploadVideoLayout.setVisibility(View.VISIBLE);
        }

        chooseImageBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
//                    askForPermission();
//                }else {
                    chooseImages();
//                }
            }
        });
    }

    private void chooseImages() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("images/");
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        startActivityForResult(intent, REQUEST_CODE_READ_STORAGE);
    }

    private void askForPermission() {
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK){
            if (requestCode == REQUEST_CODE_READ_STORAGE){
                if (data != null){
                    if (data.getClipData() != null){
                        int count = data.getClipData().getItemCount();
                        int currentItem = 0;
                        Uri imageUri = data.getClipData().getItemAt(currentItem).getUri();
                        currentItem = currentItem + 1;

                        try {
                            arrayList.add(imageUri);
                            ImagesAdapter adapter = new ImagesAdapter(ImaAudVidActivity.this, arrayList);
                            imagesList.setAdapter(adapter);
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                }else if (data.getData() != null){
                    final Uri uri = data.getData();

                    try {
                        arrayList.add(uri);
                        ImagesAdapter adapter = new ImagesAdapter(ImaAudVidActivity.this, arrayList);
                        imagesList.setAdapter(adapter);
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            }
        }
    }
}
