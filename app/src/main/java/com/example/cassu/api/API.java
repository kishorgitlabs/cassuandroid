package com.example.cassu.api;

import com.example.cassu.api.model.editprofle.EditProfle;
import com.example.cassu.api.model.login.Login;
import com.example.cassu.api.model.storecontact.StoreContact;
import com.example.cassu.api.model.storeinfo.StoreInfo;
import com.example.cassu.api.model.storeinformation.StoreInfoView;
import com.example.cassu.api.model.userimage.UploadImage;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface API {


    @FormUrlEncoded
    @POST("api/Android/Login")
    public Call<Login> login(
            @Field("MobileNo") String mobno,
            @Field("Password") String password
    );

//    @FormUrlEncoded
//    @POST("api/Android/PostUserImage")
//    public Call<UploadImage> login(
//            @Field("MobileNo") String mobno
//    );

    @FormUrlEncoded
    @POST("api/Android/StoreInfoFromCms")
    public Call<StoreInfoView> storeInfoView(
            @Field("Shopid") String shopId
    );

    @FormUrlEncoded
    @POST("api/Android/EditProfile")
    public Call<EditProfle> editProfile(
            @Field("MobileNo") String mobileNo,
            @Field("Password") String password,
            @Field("NewPassword") String newPassword,
            @Field("ShopOwnerImage") String shopOwnerimage
    );

    @FormUrlEncoded
    @POST("/api/Android/storeInfo")
    public Call<StoreInfo> storeInfo(
            @Field("ShopId") int shopId,
            @Field("TypeOfShop") String typeOfShop,
            @Field("ShopName") String shopName,
            @Field("Address") String address,
            @Field("Pincode") String pinCode,
            @Field("EmailId") String emailId,
            @Field("Website") String website,
            @Field("LandMark") String landMark,
            @Field("OpeningTime") String openingTime,
            @Field("ClosingTime") String closingTime,
            @Field("HomeDelivery") String homeDelivery,
            @Field("HDCharge") String hdCharge,
            @Field("PickupFacility") String pickUpFacility,
            @Field("QuerierFacility") String querier

    );

    @FormUrlEncoded
    @POST("/api/Android/storeContactInfo")
    public Call<StoreContact> storeContact(
            @Field("ShopId") int shopId,
            @Field("ShopName") String shopName,
            @Field("ContactPerson") String contact,
            @Field("MobileNo") String mobileNo,
            @Field("WhatUpNo") String whatsApp,
            @Field("PaytmNo") String payTm,
            @Field("AmazonPay") String amazonPay,
            @Field("Gpay") String gPay

    );

}
