package com.example.cassu.api.model.userimage;

import java.io.Serializable;

public class UploadImage implements Serializable {
	private String message;

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	@Override
 	public String toString(){
		return 
			"UploadImage{" + 
			"message = '" + message + '\'' + 
			"}";
		}
}