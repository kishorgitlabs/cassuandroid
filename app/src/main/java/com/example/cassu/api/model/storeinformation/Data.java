package com.example.cassu.api.model.storeinformation;

import java.io.Serializable;

public class Data implements Serializable {

	private Object sinfo;
	private String ShopImageURL;
	private String ShopAudioURL;
	private String ShopVideoURL;
	private int id;
	private String TypeOfShop;
	private String ShopId;
	private String ShopName;
	private String Address;
	private String Pincode;
	private String EmailId;
	private String Website;
	private String LandMark;
	private String OpeningTime;
	private String ClosingTime;
	private String HomeDelivery;
	private Object HDCharge;
	private String PickupFacility;
	private Object GpsLocation;
	private Object ShopImage;
	private String Date;
	private Object Status;
	private Object Audio;
	private Object Video;
	private Object Image1;
	private Object Image2;
	private String QuerierFacility;

	public Object getSinfo() {
		return sinfo;
	}

	public void setSinfo(Object sinfo) {
		this.sinfo = sinfo;
	}

	public String getShopImageURL() {
		return ShopImageURL;
	}

	public void setShopImageURL(String shopImageURL) {
		ShopImageURL = shopImageURL;
	}

	public String getShopAudioURL() {
		return ShopAudioURL;
	}

	public void setShopAudioURL(String shopAudioURL) {
		ShopAudioURL = shopAudioURL;
	}

	public String getShopVideoURL() {
		return ShopVideoURL;
	}

	public void setShopVideoURL(String shopVideoURL) {
		ShopVideoURL = shopVideoURL;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTypeOfShop() {
		return TypeOfShop;
	}

	public void setTypeOfShop(String typeOfShop) {
		TypeOfShop = typeOfShop;
	}

	public String getShopId() {
		return ShopId;
	}

	public void setShopId(String shopId) {
		ShopId = shopId;
	}

	public String getShopName() {
		return ShopName;
	}

	public void setShopName(String shopName) {
		ShopName = shopName;
	}

	public String getAddress() {
		return Address;
	}

	public void setAddress(String address) {
		Address = address;
	}

	public String getPincode() {
		return Pincode;
	}

	public void setPincode(String pincode) {
		Pincode = pincode;
	}

	public String getEmailId() {
		return EmailId;
	}

	public void setEmailId(String emailId) {
		EmailId = emailId;
	}

	public String getWebsite() {
		return Website;
	}

	public void setWebsite(String website) {
		Website = website;
	}

	public String getLandMark() {
		return LandMark;
	}

	public void setLandMark(String landMark) {
		LandMark = landMark;
	}

	public String getOpeningTime() {
		return OpeningTime;
	}

	public void setOpeningTime(String openingTime) {
		OpeningTime = openingTime;
	}

	public String getClosingTime() {
		return ClosingTime;
	}

	public void setClosingTime(String closingTime) {
		ClosingTime = closingTime;
	}

	public String getHomeDelivery() {
		return HomeDelivery;
	}

	public void setHomeDelivery(String homeDelivery) {
		HomeDelivery = homeDelivery;
	}

	public Object getHDCharge() {
		return HDCharge;
	}

	public void setHDCharge(Object HDCharge) {
		this.HDCharge = HDCharge;
	}

	public String getPickupFacility() {
		return PickupFacility;
	}

	public void setPickupFacility(String pickupFacility) {
		PickupFacility = pickupFacility;
	}

	public Object getGpsLocation() {
		return GpsLocation;
	}

	public void setGpsLocation(Object gpsLocation) {
		GpsLocation = gpsLocation;
	}

	public Object getShopImage() {
		return ShopImage;
	}

	public void setShopImage(Object shopImage) {
		ShopImage = shopImage;
	}

	public String getDate() {
		return Date;
	}

	public void setDate(String date) {
		Date = date;
	}

	public Object getStatus() {
		return Status;
	}

	public void setStatus(Object status) {
		Status = status;
	}

	public Object getAudio() {
		return Audio;
	}

	public void setAudio(Object audio) {
		Audio = audio;
	}

	public Object getVideo() {
		return Video;
	}

	public void setVideo(Object video) {
		Video = video;
	}

	public Object getImage1() {
		return Image1;
	}

	public void setImage1(Object image1) {
		Image1 = image1;
	}

	public Object getImage2() {
		return Image2;
	}

	public void setImage2(Object image2) {
		Image2 = image2;
	}

	public String getQuerierFacility() {
		return QuerierFacility;
	}

	public void setQuerierFacility(String querierFacility) {
		QuerierFacility = querierFacility;
	}

	@Override
	public String toString() {
		return "Data{" +
				"sinfo=" + sinfo +
				", ShopImageURL='" + ShopImageURL + '\'' +
				", ShopAudioURL='" + ShopAudioURL + '\'' +
				", ShopVideoURL='" + ShopVideoURL + '\'' +
				", id=" + id +
				", TypeOfShop='" + TypeOfShop + '\'' +
				", ShopId='" + ShopId + '\'' +
				", ShopName='" + ShopName + '\'' +
				", Address='" + Address + '\'' +
				", Pincode='" + Pincode + '\'' +
				", EmailId='" + EmailId + '\'' +
				", Website='" + Website + '\'' +
				", LandMark='" + LandMark + '\'' +
				", OpeningTime='" + OpeningTime + '\'' +
				", ClosingTime='" + ClosingTime + '\'' +
				", HomeDelivery='" + HomeDelivery + '\'' +
				", HDCharge=" + HDCharge +
				", PickupFacility='" + PickupFacility + '\'' +
				", GpsLocation=" + GpsLocation +
				", ShopImage=" + ShopImage +
				", Date='" + Date + '\'' +
				", Status=" + Status +
				", Audio=" + Audio +
				", Video=" + Video +
				", Image1=" + Image1 +
				", Image2=" + Image2 +
				", QuerierFacility='" + QuerierFacility + '\'' +
				'}';
	}
}