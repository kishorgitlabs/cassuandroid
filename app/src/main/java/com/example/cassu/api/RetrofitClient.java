package com.example.cassu.api;

import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

import static com.example.cassu.Contants.ROOT_URL;

public class RetrofitClient {

    private static Retrofit getRetrofitInstance() {
        GsonConverterFactory gsonConverterFactory=GsonConverterFactory.create(new GsonBuilder()
//                .registerTypeAdapterFactory(new NullStringToEmptyAdapterFactory())
                .create());

        return new Retrofit.Builder()
                .baseUrl(ROOT_URL)
                .client(okClient())
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(gsonConverterFactory)
                .build();
    }

    private static OkHttpClient okClient() {
        return new OkHttpClient.Builder()
                .connectTimeout(15, TimeUnit.MINUTES)
                .writeTimeout(15, TimeUnit.MINUTES)
                .readTimeout(15, TimeUnit.MINUTES)
                .build();
    }


    /**
     * Get API Service
     *
     * @return API Service
     */
    public static API getApiService() {
        return getRetrofitInstance().create(API.class);
    }
}
