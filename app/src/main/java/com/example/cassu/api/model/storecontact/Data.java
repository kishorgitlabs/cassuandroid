package com.example.cassu.api.model.storecontact;

import java.io.Serializable;

public class Data implements Serializable {
	private int id;
	private int Shopid;
	private String ShopName;
	private String ContactPerson;
	private String MobileNo;
	private String WhatUpNo;
	private String PaytmNo;
	private String GPay;
	private String AmazonPay;
	private String InsertDate;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getShopid() {
		return Shopid;
	}

	public void setShopid(int shopid) {
		Shopid = shopid;
	}

	public String getShopName() {
		return ShopName;
	}

	public void setShopName(String shopName) {
		ShopName = shopName;
	}

	public String getContactPerson() {
		return ContactPerson;
	}

	public void setContactPerson(String contactPerson) {
		ContactPerson = contactPerson;
	}

	public String getMobileNo() {
		return MobileNo;
	}

	public void setMobileNo(String mobileNo) {
		MobileNo = mobileNo;
	}

	public String getWhatUpNo() {
		return WhatUpNo;
	}

	public void setWhatUpNo(String whatUpNo) {
		WhatUpNo = whatUpNo;
	}

	public String getPaytmNo() {
		return PaytmNo;
	}

	public void setPaytmNo(String paytmNo) {
		PaytmNo = paytmNo;
	}

	public String getGPay() {
		return GPay;
	}

	public void setGPay(String GPay) {
		this.GPay = GPay;
	}

	public String getAmazonPay() {
		return AmazonPay;
	}

	public void setAmazonPay(String amazonPay) {
		AmazonPay = amazonPay;
	}

	public String getInsertDate() {
		return InsertDate;
	}

	public void setInsertDate(String insertDate) {
		InsertDate = insertDate;
	}

	@Override
	public String toString() {
		return "Data{" +
				"id=" + id +
				", Shopid=" + Shopid +
				", ShopName='" + ShopName + '\'' +
				", ContactPerson='" + ContactPerson + '\'' +
				", MobileNo='" + MobileNo + '\'' +
				", WhatUpNo='" + WhatUpNo + '\'' +
				", PaytmNo='" + PaytmNo + '\'' +
				", GPay='" + GPay + '\'' +
				", AmazonPay='" + AmazonPay + '\'' +
				", InsertDate='" + InsertDate + '\'' +
				'}';
	}
}