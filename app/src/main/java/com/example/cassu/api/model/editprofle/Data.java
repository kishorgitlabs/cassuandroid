package com.example.cassu.api.model.editprofle;

import java.io.Serializable;

public class Data implements Serializable {

	private Object setpro;
	private String ShopOwnerImages;
	private int id;
	private String ShopName;
	private String Name;
	private String MobileNo;
	private String UserName;
	private String Password;
	private String AccountStatus;
	private String StoreStatus;
	private String OtpVerification;
	private String Address;
	private String State;
	private String City;
	private String Area;
	private String Pincode;
	private String ShopOwnerImage;
	private String InsertDate;
	private String Country;
	private String RegType;
	private String TypeofShop;
	private Object priority;

	public Object getSetpro() {
		return setpro;
	}

	public void setSetpro(Object setpro) {
		this.setpro = setpro;
	}

	public String getShopOwnerImages() {
		return ShopOwnerImages;
	}

	public void setShopOwnerImages(String shopOwnerImages) {
		ShopOwnerImages = shopOwnerImages;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getShopName() {
		return ShopName;
	}

	public void setShopName(String shopName) {
		ShopName = shopName;
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public String getMobileNo() {
		return MobileNo;
	}

	public void setMobileNo(String mobileNo) {
		MobileNo = mobileNo;
	}

	public String getUserName() {
		return UserName;
	}

	public void setUserName(String userName) {
		UserName = userName;
	}

	public String getPassword() {
		return Password;
	}

	public void setPassword(String password) {
		Password = password;
	}

	public String getAccountStatus() {
		return AccountStatus;
	}

	public void setAccountStatus(String accountStatus) {
		AccountStatus = accountStatus;
	}

	public String getStoreStatus() {
		return StoreStatus;
	}

	public void setStoreStatus(String storeStatus) {
		StoreStatus = storeStatus;
	}

	public String getOtpVerification() {
		return OtpVerification;
	}

	public void setOtpVerification(String otpVerification) {
		OtpVerification = otpVerification;
	}

	public String getAddress() {
		return Address;
	}

	public void setAddress(String address) {
		Address = address;
	}

	public String getState() {
		return State;
	}

	public void setState(String state) {
		State = state;
	}

	public String getCity() {
		return City;
	}

	public void setCity(String city) {
		City = city;
	}

	public String getArea() {
		return Area;
	}

	public void setArea(String area) {
		Area = area;
	}

	public String getPincode() {
		return Pincode;
	}

	public void setPincode(String pincode) {
		Pincode = pincode;
	}

	public String getShopOwnerImage() {
		return ShopOwnerImage;
	}

	public void setShopOwnerImage(String shopOwnerImage) {
		ShopOwnerImage = shopOwnerImage;
	}

	public String getInsertDate() {
		return InsertDate;
	}

	public void setInsertDate(String insertDate) {
		InsertDate = insertDate;
	}

	public String getCountry() {
		return Country;
	}

	public void setCountry(String country) {
		Country = country;
	}

	public String getRegType() {
		return RegType;
	}

	public void setRegType(String regType) {
		RegType = regType;
	}

	public String getTypeofShop() {
		return TypeofShop;
	}

	public void setTypeofShop(String typeofShop) {
		TypeofShop = typeofShop;
	}

	public Object getPriority() {
		return priority;
	}

	public void setPriority(Object priority) {
		this.priority = priority;
	}

	@Override
	public String toString() {
		return "Data{" +
				"setpro=" + setpro +
				", ShopOwnerImages='" + ShopOwnerImages + '\'' +
				", id=" + id +
				", ShopName='" + ShopName + '\'' +
				", Name='" + Name + '\'' +
				", MobileNo='" + MobileNo + '\'' +
				", UserName='" + UserName + '\'' +
				", Password='" + Password + '\'' +
				", AccountStatus='" + AccountStatus + '\'' +
				", StoreStatus='" + StoreStatus + '\'' +
				", OtpVerification='" + OtpVerification + '\'' +
				", Address='" + Address + '\'' +
				", State='" + State + '\'' +
				", City='" + City + '\'' +
				", Area='" + Area + '\'' +
				", Pincode='" + Pincode + '\'' +
				", ShopOwnerImage='" + ShopOwnerImage + '\'' +
				", InsertDate='" + InsertDate + '\'' +
				", Country='" + Country + '\'' +
				", RegType='" + RegType + '\'' +
				", TypeofShop='" + TypeofShop + '\'' +
				", priority=" + priority +
				'}';
	}
}