package com.example.cassu.api.model.storeinformation;

import java.io.Serializable;

public class StoreInfoView implements Serializable {
	private String result;
	private Data data;

	public void setResult(String result){
		this.result = result;
	}

	public String getResult(){
		return result;
	}

	public void setData(Data data){
		this.data = data;
	}

	public Data getData(){
		return data;
	}

	@Override
 	public String toString(){
		return 
			"StoreInfoView{" + 
			"result = '" + result + '\'' + 
			",data = '" + data + '\'' + 
			"}";
		}
}