package com.example.cassu.alertbox;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.example.cassu.R;

public class AlertBox {

    private Context mContext;

    public AlertBox(Context mContext) {
        this.mContext = mContext;
    }

    public void loginSuccess(String msg){
        final AlertDialog alertDialog = new AlertDialog.Builder(
                mContext).create();

        LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.login_success, null);
        alertDialog.setView(dialogView);

        TextView textView = dialogView.findViewById(R.id.msg_success);
        Button ok = dialogView.findViewById(R.id.ok_btn);
        textView.setText(msg);

        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        Window window = alertDialog.getWindow();
        window.setLayout(600,600);
        alertDialog.show();
    }

    public void suucessWithBack(String msg){
        final AlertDialog alertDialog = new AlertDialog.Builder(
                mContext).create();

        LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.login_success, null);
        alertDialog.setView(dialogView);

        TextView textView = dialogView.findViewById(R.id.msg_success);
        Button ok = dialogView.findViewById(R.id.ok_btn);
        textView.setText(msg);

        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                ((Activity) mContext).onBackPressed();
            }
        });

        Window window = alertDialog.getWindow();
        window.setLayout(600,600);
        alertDialog.show();
    }
}
