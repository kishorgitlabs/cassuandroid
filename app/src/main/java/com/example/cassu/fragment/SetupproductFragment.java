package com.example.cassu.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.fragment.app.Fragment;

import com.example.cassu.R;
import com.example.cassu.activity.SetMainProductActivity;
import com.example.cassu.activity.SetProductItemActivity;
import com.example.cassu.activity.SetProductPriceActivity;

public class SetupproductFragment extends Fragment {

    private RelativeLayout setMainProductLayout, setProductItemLayout, setProductPriceLayout;

    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState){
        View view = inflater.inflate(R.layout.fragment_setupproduct, parent, false);

        intit(view);

        return view;
    }

    private void intit(View view) {
        setMainProductLayout = view.findViewById(R.id.set_main_product);
        setProductItemLayout = view.findViewById(R.id.set_product_item);
        setProductPriceLayout = view.findViewById(R.id.set_product_price);

        setMainProductLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), SetMainProductActivity.class);
                startActivity(intent);
            }
        });

        setProductItemLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), SetProductItemActivity.class);
                startActivity(intent);
            }
        });

        setProductPriceLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), SetProductPriceActivity.class);
                startActivity(intent);
            }
        });
    }
}
