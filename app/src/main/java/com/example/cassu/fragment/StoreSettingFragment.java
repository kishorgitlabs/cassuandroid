package com.example.cassu.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.fragment.app.Fragment;

import com.example.cassu.R;
import com.example.cassu.activity.ImaAudVidActivity;
import com.example.cassu.activity.StoreContactActivity;
import com.example.cassu.activity.StoreInfoActivity;

public class

StoreSettingFragment extends Fragment {

    private RelativeLayout storeinfo, storeContact, picsUpload, videoUpload, audioUpload;

    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState){
        View view = inflater.inflate(R.layout.fragment_store_setting, parent, false);

        initi(view);

        return view;
    }

    private void initi(View view) {
        storeinfo = view.findViewById(R.id.store_info);
        storeContact = view.findViewById(R.id.store_contact);
        picsUpload = view.findViewById(R.id.store_upload_image);
        videoUpload = view.findViewById(R.id.store_upload_video);
        audioUpload = view.findViewById(R.id.store_upload_audio);


        storeinfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), StoreInfoActivity.class);
                startActivity(intent);
            }
        });

        storeContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), StoreContactActivity.class);
                startActivity(intent);
            }
        });

        picsUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), ImaAudVidActivity.class);
                intent.putExtra("className", "Store Images");
                startActivity(intent);
            }
        });

        videoUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), ImaAudVidActivity.class);
                intent.putExtra("className", "Store Videos");
                startActivity(intent);
            }
        });

        audioUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), ImaAudVidActivity.class);
                intent.putExtra("className", "Audio Format");
                startActivity(intent);
            }
        });

    }
}
