package com.example.cassu.fragment;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.fragment.app.Fragment;

import com.example.cassu.R;
import com.example.cassu.activity.LoginActivity;
import com.example.cassu.alertbox.AlertBox;
import com.example.cassu.api.API;
import com.example.cassu.api.RetrofitClient;
import com.example.cassu.api.model.editprofle.EditProfle;
import com.google.android.material.textfield.TextInputEditText;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;

public class EditProfileFragment extends Fragment {

    private TextInputEditText shopName, name, mobileNo, password, changePassword;
    private String ShopName, Name, MobileNo, Password, ChangePassword, shopOwnerImage;
    private Button updateBtn;

    SharedPreferences myShare;
    SharedPreferences.Editor editor;

    private AlertBox box;

    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState){
        View view = inflater.inflate(R.layout.fragment_edit_profile, parent, false);

        initi(view);

        return view;
    }

    private void initi(View view) {
        shopName = view.findViewById(R.id.store_name);
        name = view.findViewById(R.id.name);
        mobileNo = view.findViewById(R.id.mobile_no_edit);
        password = view.findViewById(R.id.password);
        changePassword = view.findViewById(R.id.change_pass);
        updateBtn = view.findViewById(R.id.update_btn);
        box = new AlertBox(getActivity());
        myShare = getActivity().getSharedPreferences("CASSU", MODE_PRIVATE);

        ShopName = myShare.getString("shopName", "");
        Name = myShare.getString("name", "");
        MobileNo = myShare.getString("mobileNo", "");
        shopOwnerImage = myShare.getString("shopOwnerImage", "");
        Password = myShare.getString("password", "");


        editor = myShare.edit();


        shopName.setText(ShopName);
        name.setText(Name);
        mobileNo.setText(MobileNo);
        password.setText(Password);


        updateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ChangePassword = changePassword.getText().toString();

                if (ChangePassword.equals("")) {
                    box.loginSuccess("Enter the New Password");
                } else {

                    udateProfile();
                }
            }
        });
    }

    private void udateProfile() {
        final ProgressDialog loading = ProgressDialog.show(getActivity(), getResources().getString(R.string.app_name), "Loading...", false, false);
        API api = RetrofitClient.getApiService();

        Call<EditProfle> call = api.editProfile(MobileNo, Password, ChangePassword, shopOwnerImage);

        call.enqueue(new Callback<EditProfle>() {
            @Override
            public void onResponse(Call<EditProfle> call, Response<EditProfle> response) {
                loading.dismiss();
                if (response.body().getResult().equals("Success")){

                }else if (response.body().getResult().equals("NotSuccess")){
                    box.loginSuccess("Check Your current password and try again");
                }else if (response.body().getResult().equals("NotMatchPassword")){
                    box.loginSuccess("Check Your current password and try again");
                }else {
                    box.loginSuccess("Please try again later");
                }
            }

            @Override
            public void onFailure(Call<EditProfle> call, Throwable t) {
                loading.dismiss();
                box.loginSuccess(getString(R.string.error_server));
            }
        });
    }
}
