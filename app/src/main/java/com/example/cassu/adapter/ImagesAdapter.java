package com.example.cassu.adapter;

import android.content.Context;
import android.net.Uri;
import android.os.FileUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.cassu.R;

import java.util.ArrayList;

public class ImagesAdapter extends BaseAdapter {

    private Context mContext;
    private ArrayList<Uri> arrayList;

    public ImagesAdapter(Context mContext, ArrayList<Uri> arrayList) {
        this.mContext = mContext;
        this.arrayList = arrayList;
    }

    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return arrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        convertView = inflater.inflate(R.layout.images_view_layout, parent, false);

        ImageView storeImage = convertView.findViewById(R.id.store_image);
        TextView imagePath = convertView.findViewById(R.id.image_path);

        imagePath.setText(arrayList.get(position).getUserInfo());

        Glide.with(mContext)
                .load(arrayList.get(position))
                .into(storeImage);

        return convertView;
    }
}
